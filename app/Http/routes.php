<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::get('/test', function()
{
    return view('ajaxtest.base');
});

Route::get('/test_ajax', function()
{
    $out=env('TST_VAR', 'Default Value');
    if (Auth::guest())
        return $out. ' : Guest';
    return $out. ' : User';
});